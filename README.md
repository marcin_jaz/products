Call 
```mvn springBoot:run```
and Swagger will be present under http://localhost:8080/swagger-ui.html

It uses in memory database, could be easily changed to some persistent storage.