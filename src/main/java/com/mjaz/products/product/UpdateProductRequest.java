package com.mjaz.products.product;

import java.util.UUID;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
public class UpdateProductRequest extends AddProductRequest {

    @NotNull
    private UUID id;

    @Builder(builderMethodName = "builderUpdate")
    public UpdateProductRequest(@NotNull @NotEmpty String name,
        @NotNull @Valid PriceDto price, @NotNull UUID id) {
        super(name, price);
        this.id = id;
    }
}
