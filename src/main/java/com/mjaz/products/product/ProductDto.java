package com.mjaz.products.product;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mjaz.products.product.domain.Product;
import java.sql.Timestamp;
import java.util.UUID;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class ProductDto {

    private final UUID id;
    private final String name;
    private final PriceDto price;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss.SSS")
    private final Timestamp createdDate;

    public static ProductDto createFromModel(Product product) {
        return ProductDto.builder().id(product.getId()).name(product.getName()).price(PriceDto.createFromModel(product.getPrice()))
            .createdDate(product.getCreatedDate()).build();
    }
}
