package com.mjaz.products.product.domain;

import java.sql.Timestamp;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Where;

@Builder
@Entity
@Table(name = "products")
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Where(clause = "active=1")
public class Product {

    @Id
    private UUID id;
    @Column
    private String name;
    @Column
    private Price price;
    @CreationTimestamp
    @EqualsAndHashCode.Exclude
    private Timestamp createdDate;
    @Column(name = "active", columnDefinition = "tinyint(1) default 1")
    @Builder.Default
    private boolean active = true;
    @Version
    private Integer version;

}
