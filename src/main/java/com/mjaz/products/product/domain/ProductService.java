package com.mjaz.products.product.domain;

import com.mjaz.products.product.AddProductRequest;
import com.mjaz.products.product.UpdateProductRequest;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public Product add(AddProductRequest addProductRequest) {
        Product product = Product.builder().id(UUID.randomUUID()).name(addProductRequest.getName())
            .price(Price.fromApiPrice(addProductRequest.getPrice())).build();
        return productRepository.save(product);
    }

    public List<Product> getAll(int page, int size) {
        Page<Product> result = productRepository.findAll(PageRequest.of(page, size));
        return result.getContent();
    }

    public void delete(UUID id) {
        productRepository.softDelete(id);
    }

    public Optional<Product> findById(UUID id) {
        return productRepository.findById(id);
    }

    @Transactional
    public Product update(UpdateProductRequest addProductRequest) {
        Product product = productRepository.getOne(addProductRequest.getId());
        product.setName(addProductRequest.getName());
        product.setPrice(Price.fromApiPrice(addProductRequest.getPrice()));
        return productRepository.save(product);
    }
}
