package com.mjaz.products.product.domain;

import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
interface ProductRepository extends JpaRepository<Product, UUID> {

    @Query("update Product set active=0 where id = :id")
    @Transactional
    @Modifying
    void softDelete(@Param("id") UUID uuid);
}
