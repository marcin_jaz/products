package com.mjaz.products.product.domain;

import com.mjaz.products.product.PriceDto;
import java.math.BigDecimal;
import java.util.Currency;
import javax.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Embeddable
@Getter
@NoArgsConstructor
public class Price {

    private BigDecimal amount;
    private Currency currency;

    public static Price fromApiPrice(PriceDto price) {
        return new Price(price.getAmount(), price.getCurrency());
    }
}
