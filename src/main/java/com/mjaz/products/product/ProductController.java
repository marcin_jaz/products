package com.mjaz.products.product;

import com.mjaz.products.product.domain.Product;
import com.mjaz.products.product.domain.ProductService;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @PutMapping
    public ResponseEntity<ProductDto> addProduct(@Valid @RequestBody AddProductRequest addProductRequest) {
        Product product = productService.add(addProductRequest);
        ProductDto productDto = ProductDto.createFromModel(product);
        return ResponseEntity.ok(productDto);
    }

    @PostMapping
    public ResponseEntity<ProductDto> updateProduct(@Valid @RequestBody UpdateProductRequest updateProductRequest) {
        Product product = productService.update(updateProductRequest);
        ProductDto productDto = ProductDto.createFromModel(product);
        return ResponseEntity.ok(productDto);
    }

    @GetMapping("/list")
    public ResponseEntity<List<ProductDto>> getProducts(@RequestParam(required = false, defaultValue = "0") int page,
        @RequestParam(required = false, defaultValue = "100") int size) {
        List<Product> products = productService.getAll(page, size);
        List<ProductDto> result = products.stream().map(ProductDto::createFromModel).collect(Collectors.toList());
        return ResponseEntity.ok(result);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteProduct(@PathVariable("id") @NotEmpty UUID id) {
        productService.delete(id);
        return ResponseEntity.accepted().build();
    }

}
