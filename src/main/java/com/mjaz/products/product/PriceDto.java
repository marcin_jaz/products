package com.mjaz.products.product;

import com.mjaz.products.product.domain.Price;
import java.math.BigDecimal;
import java.util.Currency;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@AllArgsConstructor
@Getter
@EqualsAndHashCode
public class PriceDto {

    public static PriceDto createInDollars(BigDecimal amount) {
        return new PriceDto(amount, Currency.getInstance("USD"));
    }

    public static PriceDto createInFrank(BigDecimal amount) {
        return new PriceDto(amount, Currency.getInstance("CHF"));
    }

    private final BigDecimal amount;
    private final Currency currency;

    public static PriceDto createFromModel(Price price) {
        return new PriceDto(price.getAmount(), price.getCurrency());
    }
}
