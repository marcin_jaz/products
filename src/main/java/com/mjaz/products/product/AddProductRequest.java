package com.mjaz.products.product;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@Builder
@EqualsAndHashCode
public class AddProductRequest {

    @NotNull
    @NotEmpty
    private String name;
    @NotNull
    @Valid
    private PriceDto price;

}
