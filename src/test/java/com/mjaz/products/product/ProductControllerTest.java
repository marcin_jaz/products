package com.mjaz.products.product;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.mjaz.products.AbstractControllerTest;
import com.mjaz.products.product.domain.Price;
import com.mjaz.products.product.domain.Product;
import com.mjaz.products.product.domain.ProductService;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Currency;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

class ProductControllerTest extends AbstractControllerTest {

    @MockBean
    private ProductService productService;

    @Test
    void shouldAddProduct() throws Exception {
        //given
        AddProductRequest request = AddProductRequest.builder().name("product")
            .price(PriceDto.createInDollars(BigDecimal.TEN)).build();
        Product product = Product.builder().name("product1")
            .price(new Price(BigDecimal.TEN, Currency.getInstance("USD")))
            .id(UUID.randomUUID())
            .createdDate(new Timestamp(System.currentTimeMillis()))
            .build();
        when(productService.add(request)).thenReturn(product);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
            .put("/api/product")
            .contentType(APPLICATION_JSON)
            .content(asJsonString(request));
        ProductDto productDto = ProductDto.builder().name(product.getName()).id(product.getId()).createdDate(product.getCreatedDate())
            .price(new PriceDto(product.getPrice().getAmount(), product.getPrice().getCurrency())).build();
        //when
        mockMvc.perform(requestBuilder)
            //then
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON))
            .andExpect(content().string(asJsonString(productDto)));
    }

    @Test
    void shouldNotAllowToAddProductWithoutName() throws Exception {
        //given
        AddProductRequest request = AddProductRequest.builder()
            .price(new PriceDto(BigDecimal.TEN, Currency.getInstance("USD"))).build();
        RequestBuilder requestBuilder = MockMvcRequestBuilders
            .put("/api/product")
            .contentType(APPLICATION_JSON)
            .content(asJsonString(request));
        //when
        mockMvc.perform(requestBuilder)
            //then
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldNotAllowToAddProductWithoutPrice() throws Exception {
        //given
        AddProductRequest request = AddProductRequest.builder().name("product").build();
        RequestBuilder requestBuilder = MockMvcRequestBuilders
            .put("/api/product")
            .contentType(APPLICATION_JSON)
            .content(asJsonString(request));
        //when
        mockMvc.perform(requestBuilder)
            //then
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldGetTwoProducts() throws Exception {
        //given
        Product product = Product.builder().name("product")
            .price(new Price(BigDecimal.TEN, Currency.getInstance("USD")))
            .id(UUID.randomUUID())
            .createdDate(new Timestamp(System.currentTimeMillis()))
            .build();
        when(productService.getAll(0, 2)).thenReturn(Arrays.asList(product, product));

        RequestBuilder requestBuilder = MockMvcRequestBuilders
            .get("/api/product/list")
            .param("page", "0")
            .param("size", "2");
        //when
        mockMvc.perform(requestBuilder)
            //then
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON))
            .andExpect(jsonPath("$.*", hasSize(2)));
    }

    @Test
    void shouldDeleteProduct() throws Exception {
        //given
        UUID productId = UUID.randomUUID();
        RequestBuilder requestBuilder = MockMvcRequestBuilders
            .delete("/api/product/{id}", productId);
        //when
        mockMvc.perform(requestBuilder)
            //then
            .andExpect(status().isAccepted());
        verify(productService).delete(productId);
    }
}