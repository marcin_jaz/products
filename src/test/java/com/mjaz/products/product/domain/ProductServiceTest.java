package com.mjaz.products.product.domain;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.mjaz.products.ProductsApplication;
import com.mjaz.products.product.AddProductRequest;
import com.mjaz.products.product.PriceDto;
import com.mjaz.products.product.UpdateProductRequest;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import org.assertj.core.internal.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProductsApplication.class)
class ProductServiceTest {

    @Autowired
    private ProductService productService;

    @Test
    void shouldAddProduct() {
        //given
        AddProductRequest request = AddProductRequest.builder().name(randomString())
            .price(PriceDto.createInDollars(BigDecimal.TEN)).build();
        //when
        Product addedProduct = productService.add(request);
        //then
        assertThat(addedProduct).isNotNull();
        assertThat(addedProduct.getName()).isEqualTo(request.getName());
        assertThat(addedProduct.getPrice().getAmount()).isEqualTo(BigDecimal.TEN);
        assertThat(addedProduct.getPrice().getCurrency()).isEqualTo(request.getPrice().getCurrency());
        assertThat(addedProduct.getCreatedDate()).isNotNull();
        assertTrue(addedProduct.isActive());
    }

    @Test
    void shouldGetPageOfProducts() {
        //assume
        addRandomProduct();
        addRandomProduct();
        addRandomProduct();
        //when
        List<Product> products = productService.getAll(0, 2);
        //then
        assertThat(products).hasSize(2);
    }

    @Test
    void shouldFindProductAfterItsAdded() {
        //given
        Product product = addRandomProduct();
        //when
        Optional<Product> foundProductAfterDelete = productService.findById(product.getId());
        //then
        assertTrue(foundProductAfterDelete.isPresent());
    }

    @Test
    void shouldUpdateAddedProductName() {
        //assume
        Product product = addRandomProduct();
        //given
        UpdateProductRequest request = UpdateProductRequest.builderUpdate().id(product.getId()).name("productUpdated")
            .price(PriceDto.createInFrank(BigDecimal.ONE)).build();
        //when
        Product updatedProduct = productService.update(request);
        //then
        assertThat(updatedProduct.getId()).isEqualTo(request.getId());
        assertThat(updatedProduct.getName()).isEqualTo(request.getName());
        assertThat(updatedProduct.getPrice().getAmount()).isEqualTo(request.getPrice().getAmount());
        assertThat(updatedProduct.getPrice().getCurrency()).isEqualTo(request.getPrice().getCurrency());
    }

    @Test
    void shouldNotFindAddedProductAfterItsDeleted() {
        //given
        Product product = addRandomProduct();
        //when
        productService.delete(product.getId());
        //then
        Optional<Product> foundProductAfterDelete = productService.findById(product.getId());
        assertFalse(foundProductAfterDelete.isPresent());
    }

    private Product addRandomProduct() {
        AddProductRequest request1 = AddProductRequest.builder().name(randomString())
            .price(PriceDto.createInDollars(BigDecimal.TEN)).build();
        return productService.add(request1);
    }

    private String randomString() {
        return RandomString.make();
    }
}